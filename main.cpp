#include <iostream>
#include "header/grid.h"
#include "header/TileMap.h"
#include <string>
#include <windows.h>

using namespace std;

int main() {
    Grid* grid = new Grid();
    square * spawn = grid->generateMap();
    queen* q = grid->SetUpQueen(spawn);
    int time=0;
    int total_ants = 1;

    /**Graphique Part**/
    int cursorX =82;
    int cursorY =85;

    sf::RenderWindow window(sf::VideoMode(800, 584), "AntsSimulator");

    int ** level = grid->getDisplayableGrid(cursorX,cursorY);//80 - 32
    TileMap map;
    if (!map.load("tileset.png", sf::Vector2u(16, 16), level, 50, 35))
        return -1;

    sf::Text text;
    sf::Font font;
    if (!font.loadFromFile("Montserrat-Black.ttf"))
        std::cout << "error";
    text.setFont(font);
    text.setString(" Position X :  "+to_string(cursorX)+"  Position Y :  "+to_string(cursorY)+"   Time :  "+to_string(time)+"   Food:  "+to_string(spawn->getUnitFoodCount())+"   Ants:  "+to_string(total_ants));
    text.setCharacterSize(13);
    text.setFillColor(sf::Color::White);
    text.setPosition(10,564);
    /**Graphique Part**/



    // on fait tourner la boucle principale
    while (window.isOpen())
    {
        if(!q->isAlive()){

            text.setString("The simulation is Over the queen has died after "+to_string(time)+" hours");
            level = grid->getDisplayableGrid(85,85);


            window.clear();
            window.draw(map);
            window.draw(text);
            window.display();
            Sleep(5000);
            window.close();
            break;
        }
        Sleep(30);
        time += 1;
        //Update Time
        text.setString(" Position X :  "+to_string(cursorX)+"  Position Y :  "+to_string(cursorY)+"   Time :  "+to_string(time)+"   Food:  "+to_string(spawn->getUnitFoodCount())+"   Ants:  "+to_string(total_ants));

        total_ants = 0;
        for (int i = 0; i < colNum; ++i) {
            for (int j = 0; j < rowNum; ++j) {
                square * p = grid->getSquare(i,j);
                p->evaporation();
                if(p->getNbAnts() != 0){
                    total_ants += p->getNbAnts();
                    for (auto &current_ant : p->getListOfAnts()){
                        current_ant->action();
                        if(time%24==0){
                            current_ant->eat(spawn);
                        }
                    }
                }
            }
        }
        grid->extendSpawn(total_ants,spawn);
        // on gère les évènements
        sf::Event event;
        while (window.pollEvent(event))
        {
            if(event.type == sf::Event::Closed)
                window.close();

            if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Up){
                    if(cursorY - 10 >= 0){
                        cursorY -= 10;
                    }else{
                        cursorY = 0;
                    }
                }else if (event.key.code == sf::Keyboard::Down){
                    if(cursorY+35 +10 < rowNum){
                        cursorY += 10;
                    }else{
                        cursorY = rowNum-35;
                    }
                }else if (event.key.code == sf::Keyboard::Left){
                    if(cursorX -10 >= 0){
                        cursorX -= 10;
                    }else{
                        cursorX = 0;
                    }
                }else if (event.key.code == sf::Keyboard::Right){
                    if(cursorX+50 +10 < colNum){
                        cursorX += 10;
                    }else{
                        cursorX = colNum-50;
                    }
                }
                text.setString(" Position X :  "+to_string(cursorX)+"  Position Y :  "+to_string(cursorY)+"   Time :  "+to_string(time)+"   Food:  "+to_string(spawn->getUnitFoodCount())+"   Ants:  "+to_string(total_ants));
            }

        }
        level = grid->getDisplayableGrid(cursorX,cursorY);
        if (!map.load("tileset.png", sf::Vector2u(16, 16), level, 50, 35))
            return -1;
        window.clear();
        window.draw(map);
        window.draw(text);
        window.display();
    }
}
