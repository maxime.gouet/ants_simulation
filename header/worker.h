//
// Created by FU529AFA on 23/11/2020.
//

#ifndef FOURMIS_WORKER_H
#define FOURMIS_WORKER_H
#define quantity 0.05
#include <stack>
#include "ants.h"
#include "spawn_square.h"

//mineur pendant 15 jours
class worker : public ants {

protected:
    std::stack<square *> memory;
    bool underage;

    int pheromones;
    int food;
public:
    worker();
    int getAges();
    worker(square * Case);
    void death();
    void eat(square* colonie);
    ~worker()=default;
    void setUnderage();
    void move();
    void action();
    int getType() override;


};


#endif //FOURMIS_WORKER_H
