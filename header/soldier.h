//
// Created by FU529AFA on 23/11/2020.
//

#ifndef FOURMIS_SOLDIER_H
#define FOURMIS_SOLDIER_H


#include <stack>
#include "ants.h"
#include "spawn_square.h"
#define goBackLimit 100
class soldier : public ants{
protected:
    int nbHourBeforeGoBack;
    std::stack<square *> memory;
    bool goback;
public :

    soldier();
    soldier(square * Case);
    int getAges();
    void eat(square* colonie);
    ~soldier()=default;
    bool Kill();
    void death();
    void move();
    void action();
    int getType() override;

};


#endif //FOURMIS_SOLDIER_H
