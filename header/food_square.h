//
// Created by Admin on 25/11/2020.
//

#ifndef FOURMIS_FOOD_SQUARE_H
#define FOURMIS_FOOD_SQUARE_H
#include "square.h"

class food_square : public square{
    int food_count;


public:
    food_square(int food_count);

    void setUnitFoodCount(float foodCount);
    float getUnitFoodCount();
    void down();

    int getType() override;

};


#endif //FOURMIS_FOOD_SQUARE_H
