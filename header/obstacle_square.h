//
// Created by Admin on 25/11/2020.
//

#ifndef FOURMIS_OBSTACLE_SQUARE_H
#define FOURMIS_OBSTACLE_SQUARE_H
#include "square.h"

class obstacle_square : public square{
public:
    obstacle_square();

    int getType() override;
};


#endif //FOURMIS_OBSTACLE_SQUARE_H
