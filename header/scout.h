//
// Created by FU529AFA on 23/11/2020.
//

#ifndef FOURMIS_SCOUT_H
#define FOURMIS_SCOUT_H



#include "spawn_square.h"
#include "worker.h"


class scout : public worker {
public:

    scout();
    scout(square * Case);
    void setUnderage();
    int getAges();
    void eat(square* colonie);
    void move();
    void action();
    void death();
    int getType() override;
    ~scout()= default;
};


#endif //FOURMIS_SCOUT_H
