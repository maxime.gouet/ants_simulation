//
// Created by Admin on 23/11/2020.
//

#ifndef FOURMIS_FRAME_H
#define FOURMIS_FRAME_H
#
#include <vector>
#include "ants.h"
#define EVAPORATION_VALUE 0.70

using namespace std;

class square {

protected:
    int maxAnts;
    int maxTrace;
    bool visited;
    int nbTrace;
    int nbAnts;
    vector<ants*> listOfAnts;
    vector<square*> neighbour;

public:
    void setNbAnts(int nbAnts);
    int getMaxAnts();
    void evaporation();
    int getNbAnts();
    void setVisited(bool visited);
    bool isVisited();
    void addAntsToList(ants* newAnts);
    void delAntsToList(ants * delAnts);
    virtual float getUnitFoodCount();
    virtual void setUnitFoodCount(float foodCount);
    int getnbTrace();
    int getMaxTrace();
    void setnbTrace(int nb_Trace);
    vector<square*> getneighbour();
    ~square() = default;
    virtual int getType() = 0;
    vector<ants*> getListOfAnts();
    void addNeighbour(square* neighbour);
    void resetNeighbour();
};


#endif //FOURMIS_FRAME_H
