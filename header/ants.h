//
// Created by FU529AFA on 23/11/2020.
//

#ifndef FOURMIS_ANTS_H
#define FOURMIS_ANTS_H




//#include "square.h"
//#include "spawn_square.h"
class square;


class ants{
protected:

    int ages;
    square * currentCase;
public :


    virtual void action()=0;
    int getAges();
    virtual void eat(square* colonie)=0;
    virtual int getType()=0;

};


#endif //FOURMIS_ANTS_H
