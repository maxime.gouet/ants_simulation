//
// Created by Admin on 23/11/2020.
//

#ifndef FOURMIS_GRID_H
#define FOURMIS_GRID_H
#include "square.h"
#include "queen.h"


#define rowNum 201
#define colNum 211


class Grid {
protected:
    square* grid[colNum][rowNum];
    int spawn_size;
    int spawn_index;

public:
    Grid();
    square* generateMap();
    int ** getDisplayableGrid(int startx,int starty);
    void GenerateBlocks(int nbBlocks,int lengthBlocks);
    queen* SetUpQueen(square* spawn_square);
    square * getSquare(int x,int y);
    int extendSpawn(int nbAnts,square * spawn);
    void set_neighbour();
};


#endif //FOURMIS_GRID_H
