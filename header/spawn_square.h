//
// Created by Admin on 25/11/2020.
//

#ifndef FOURMIS_SPAWN_SQUARE_H
#define FOURMIS_SPAWN_SQUARE_H

#include "square.h"


class spawn_square : public square{
protected:
    float unitFood_count;
public:
    spawn_square();
    spawn_square(std::vector<ants*> listOfAnts,std::vector<square*> neighbour,int nbAnts);
    float getUnitFoodCount();
    void setUnitFoodCount(float unitFoodCount);

    int getType() override;
};


#endif //FOURMIS_SPAWN_SQUARE_H
