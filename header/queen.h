//
// Created by FU529AFA on 23/11/2020.
//

#ifndef FOURMIS_QUEEN_H
#define FOURMIS_QUEEN_H


#include "ants.h"
#include "spawn_square.h"

#define PROBA_OUVRIER 80
#define PROBA_SOLDAT 15
#define PROBA_ECLAIREUR 5
class queen : public ants {
protected:
    bool alive;
public:

    void action();
    queen();
    int getAges();
    queen(square * Case);
    void eat(square* colonie);
    void death();
    int isAlive();
    void creatNewAnt();
    bool mugged=false;
    int getType() override;

};


#endif //FOURMIS_QUEEN_H
