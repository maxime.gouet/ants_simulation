//
// Created by Admin on 25/11/2020.
//

#ifndef FOURMIS_BLANK_SQUARE_H
#define FOURMIS_BLANK_SQUARE_H
#include "square.h"


class blank_square : public square{
public:
    blank_square();
    int getType() override;
};


#endif //FOURMIS_BLANK_SQUARE_H
