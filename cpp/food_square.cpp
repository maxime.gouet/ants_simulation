//
// Created by Admin on 25/11/2020.
//

#include "../header/food_square.h"

food_square::food_square(int food_count){
    maxAnts = 12;
    maxTrace=1000;
    visited = false;
    nbTrace = 0;
    nbAnts = 0;
    this->food_count = food_count;
}

float food_square::getUnitFoodCount()  {
    return food_count;
}

void food_square::setUnitFoodCount(float foodCount) {
    food_count = foodCount;
}
int food_square::getType(){
    return 1;
}
void food_square::down(){
    food_count=food_count-1;
}
