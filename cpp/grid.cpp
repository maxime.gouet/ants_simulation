//
// Created by Admin on 23/11/2020.
//

#include "../header/grid.h"
#include "../header/obstacle_square.h"
#include "../header/food_square.h"
#include "../header/blank_square.h"
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <tgmath.h>

Grid::Grid() {
    spawn_size = 0;
    spawn_index = 1;
}
square* Grid::generateMap() {
    int nbObstacle = rowNum*colNum *0.3;
    int nbFood = rowNum*colNum *0.02;
    srand(time(NULL));

    int nbObstacle1 = nbObstacle*0.5;
    int nbObstacle2 = nbObstacle*0.3;
    int nbObstacle3 = nbObstacle*0.1;
    int nbObstacle4 = nbObstacle*0.05;
    int nbObstacle5 = nbObstacle*0.04;
    int nbObstacle6 = nbObstacle*0.01;

    int Rx;
    int Ry;
    for (int i = 0; i < colNum; ++i) {
        for (int j = 0; j < rowNum; ++j) {
            grid[i][j] = new blank_square();
        }
    }
    GenerateBlocks(nbObstacle6,6);
    GenerateBlocks(nbObstacle5,5);
    GenerateBlocks(nbObstacle4,4);
    GenerateBlocks(nbObstacle3,3);
    GenerateBlocks(nbObstacle2,2);
    GenerateBlocks(nbObstacle1,1);

    for (int i = 0; i < nbFood; ++i) {
        Rx = rand() % rowNum;
        Ry = rand() % colNum;
        delete grid[Ry][Rx];
        grid[Ry][Rx] = new food_square(10);
    }
    // placement des gros stock en base a droite de la map
    for (int i = 0; i < 2; ++i) {
        Rx = rowNum-11 + rand() % 10;
        Ry = colNum-11 + rand() % 10;

        delete grid[Ry+1][Rx];
        delete grid[Ry-1][Rx];
        delete grid[Ry][Rx+1];
        delete grid[Ry][Rx-1];
        delete grid[Ry][Rx];
        grid[Ry+1][Rx] = new food_square(20000);
        grid[Ry-1][Rx] = new food_square(20000);
        grid[Ry][Rx+1] = new food_square(20000);
        grid[Ry][Rx-1] = new food_square(20000);
        grid[Ry][Rx] = new food_square(20000);
    }

    Rx = (rowNum+1) / 2;
    Ry = (colNum+1) / 2;
    delete grid[Ry][Rx];
    grid[Ry][Rx] = new spawn_square();

    //Remove the stone around the spawn
    for(int y=Ry-3;y<Ry+3;y++){
        for(int x=Rx-3;x<Rx+3;x++){
            if(grid[y][x]->getType() == 2){
                delete grid[y][x];
                grid[y][x] = new blank_square();
            }
        }
    }
    set_neighbour();

    //Return spawn_square
    return grid[Ry][Rx];
}
int ** Grid::getDisplayableGrid(int startx,int starty){
    int** currentgrid = 0;
    currentgrid = new int*[50];
    for (int h = 0; h < 50; h++)
    {
        currentgrid[h] = new int[35];
    }

    for (int i = startx; i < 50+startx; ++i) {
        for (int j = starty; j <35+starty; ++j) {
            if(grid[i][j]->getType() == 2){
                currentgrid[i-startx][j-starty] = 2;
            }else if(grid[i][j]->getType() == 1){

                if(grid[i][j]->getNbAnts() > 0){
                    if(grid[i][j]->getListOfAnts()[0]->getType() == 1){
                        currentgrid[i-startx][j-starty] = 7;
                    }else if(grid[i][j]->getListOfAnts()[0]->getType() == 3) {
                        currentgrid[i-startx][j-starty] = 9;
                    }else{
                        currentgrid[i-startx][j-starty] = 5;
                    }
                }else{
                    currentgrid[i-startx][j-starty] = 1;
                }
            }else if(grid[i][j]->getType() == 0){

                if(grid[i][j]->getNbAnts() > 0){
                    if(grid[i][j]->getListOfAnts()[0]->getType() == 1){
                        currentgrid[i-startx][j-starty] = 6;
                    }else if(grid[i][j]->getListOfAnts()[0]->getType() == 3) {
                        currentgrid[i-startx][j-starty] = 8;
                    }else{
                        currentgrid[i-startx][j-starty] = 4;
                    }
                }else{
                    currentgrid[i-startx][j-starty] = 0;
                }
            }else if(grid[i][j]->getType() == 3){
                currentgrid[i-startx][j-starty] = 3;
            }
        }
    }
    return currentgrid;
}
void Grid::GenerateBlocks(int nbBlocks,int lengthBlocks){
    for (int i = 0; i < nbBlocks; ++i) {
        int Rx = rand() % rowNum;
        int Ry = rand() % colNum;
        grid[Ry][Rx] = new obstacle_square();
        for(int x=0;x<lengthBlocks-1;x++) {
            int Rdirection = rand() % 4;
            switch (Rdirection) {
                case 0:
                    if(Ry+1 < colNum) {
                        Ry += 1;
                    }else{
                        //Sinon refaire une iteration
                        x--;
                    }
                    break;
                case 1:
                    if(Rx+1 < rowNum) {
                        Rx += 1;
                    }else{
                        //Sinon refaire une iteration
                        x--;
                    }
                    break;
                case 2:
                    if(Ry-1 > 0) {
                        Ry -= 1;
                    }else{
                        x--;
                    }
                    break;
                case 3:
                    if(Rx-1 > 0){
                        Rx -= 1;
                    }else{
                        x--;
                    }
                    break;
                default:
                    break;
            }
            delete grid[Ry][Rx];
            grid[Ry][Rx] = new obstacle_square();
        }
        delete grid[Ry][Rx];
        grid[Ry][Rx] = new obstacle_square();
    }
}
queen* Grid::SetUpQueen(square* spawn_square){
    queen* q = new queen(spawn_square);
    spawn_square->addAntsToList(q);
    spawn_square->setNbAnts(spawn_square->getNbAnts()+1);
    return q;
}
square * Grid::getSquare(int x,int y){
    return grid[x][y];
}
int Grid::extendSpawn(int nbAnts,square * spawn){
    if ((floor(nbAnts/100) > spawn_size)) {
        int added = false;
        for(int y=-spawn_index;y<=spawn_index;y++){
            for(int x=-spawn_index;x<=spawn_index;x++){
                if(grid[((colNum+1) / 2)+y][((rowNum+1) / 2)+x]->getType() == 0){
                    grid[((colNum+1) / 2)+y][((rowNum+1) / 2)+x] = new spawn_square(grid[((colNum+1) / 2)+y][((rowNum+1) / 2)+x]->getListOfAnts(),grid[((colNum+1) / 2)+y][((rowNum+1) / 2)+x]->getneighbour(),grid[((colNum+1) / 2)+y][((rowNum+1) / 2)+x]->getNbAnts());
                    set_neighbour();
                    spawn_size += 1;
                    added = true;
                    return 1;
                }
            }
        }
        if(!added){
            spawn_index++;
            return 0;
        }
    }
}
void Grid::set_neighbour(){
    for (int i = 0; i < colNum; ++i) {
        for (int j = 0; j < rowNum; ++j) {
            grid[i][j]->resetNeighbour();
            for(int x=-1;x <= 1;x++){
                for(int y=-1;y <= 1;y++){
                    if(i+x >= 0 && i+x < colNum && j+y >= 0 && j+y < rowNum && (i+x != i || j+y != j)){
                        grid[i][j]->addNeighbour(grid[i+x][j+y]);
                    }
                }
            }
        }
    }
}

