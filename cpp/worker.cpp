//
// Created by FU529AFA on 23/11/2020.
//

#include "../header/worker.h"
#include <iostream>



using namespace std;
worker::worker(){
    ages=0;
    food=0;
    underage=true;
    pheromones=500;
}

worker::worker(square * Case){
    ages=0;
    food=0;
    underage=true;
    pheromones=500;
    currentCase=Case;
}
void worker::eat(square* colonie){
    if(colonie->getUnitFoodCount()>=0.001){
        colonie->setUnitFoodCount(colonie->getUnitFoodCount()-0.001);
    }else{
        death();
    }


}
void worker::move() {
    int index=10;
    bool mooved=false;
    vector<int> listeIndex;
    int i=0;


    if(food ==0){
        for(i=0; i<currentCase->getneighbour().size(); i++){
            if(currentCase->getneighbour()[i]->getType()!=2){
                if(currentCase->getneighbour()[i]->isVisited() && currentCase->getneighbour()[i]->getNbAnts()<currentCase->getneighbour()[i]->getMaxAnts()){
                    if(index==10){
                        if(currentCase->getneighbour()[i]->getnbTrace()>0){
                            index=i;
                        }

                    }else{
                        if(currentCase->getneighbour()[i]->getnbTrace()>0 && currentCase->getneighbour()[i]->getnbTrace()>currentCase->getneighbour()[index]->getnbTrace()){
                            index=i;

                        }
                    }


                }
            }
        }
        if(index==10){
            for( i=0; i<currentCase->getneighbour().size(); i++){
                if(currentCase->getneighbour()[i]->getType()!=2 && currentCase->getneighbour()[i]->isVisited() ){
                    listeIndex.push_back(i);


                }
            }
        }

        if(index==10){
            if(listeIndex.size()==0){
                currentCase->delAntsToList(this);
                currentCase->addAntsToList(this);
                mooved=true;

            }else{
                index= rand() % listeIndex.size();
                memory.push(currentCase);
                currentCase->delAntsToList(this);
                currentCase->setNbAnts(currentCase->getNbAnts()-1);
                currentCase=currentCase->getneighbour()[listeIndex[index]];
                currentCase->addAntsToList(this);
                currentCase->setNbAnts(currentCase->getNbAnts()+1);
            }

        }else{
            memory.push(currentCase);
            currentCase->delAntsToList(this);
            currentCase->setNbAnts(currentCase->getNbAnts()-1);
            currentCase=currentCase->getneighbour()[index];
            currentCase->addAntsToList(this);
            currentCase->setNbAnts(currentCase->getNbAnts()+1);
        }



        if(currentCase->getType()==1 ){

            if(currentCase->getUnitFoodCount()>0){

                currentCase->setUnitFoodCount(currentCase->getUnitFoodCount() - 1);
                food=1;

                if(currentCase->getnbTrace()<currentCase->getMaxTrace()){
                    currentCase->setnbTrace(currentCase->getnbTrace()+ (pheromones*quantity));
                    pheromones=pheromones - (pheromones*quantity);
                }

            }

        }

    }else{

        if(!memory.empty()){
            currentCase->setNbAnts(currentCase->getNbAnts()-1);
            currentCase->delAntsToList(this);
            currentCase=memory.top();
            memory.pop();
            currentCase->setNbAnts(currentCase->getNbAnts()+1);
            currentCase->addAntsToList(this);
            if(currentCase->getnbTrace()+(pheromones*quantity) < currentCase->getMaxTrace()){
                currentCase->setnbTrace(currentCase->getnbTrace()+ (pheromones*quantity));
                pheromones=pheromones*quantity;
            }

        }else{
            currentCase->setUnitFoodCount(currentCase->getUnitFoodCount()+food);
            food=0;
        }
    }

}
void worker::action() {
    ages++;
    if(ages==24*365){
        death();
    }else{
        setUnderage();

        if(!underage){
            move();
        }
    }


}
void worker::death(){
    currentCase->delAntsToList(this);
    currentCase->setNbAnts(currentCase->getNbAnts()-1);
    delete this;
}
void worker::setUnderage(){
    if(ages>=15*24){
        underage=false;
    }

}
int worker::getAges(){
    return ages;
}
int worker::getType(){
    return 2;
}


