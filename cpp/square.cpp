//
// Created by Admin on 23/11/2020.
//

#include <algorithm>
#include "../header/square.h"

using namespace std;

bool square::isVisited(){
    return visited;
}
void square::setVisited(bool visited) {
    square::visited = visited;
}

void square::setNbAnts(int nbAnts) {
    square::nbAnts = nbAnts;
}
void square::addNeighbour(square* neighbour) {
    square::neighbour.push_back(neighbour);
}
float square::getUnitFoodCount() {
    return 0;
}
void square::setUnitFoodCount(float foodCount) {

}
int square::getnbTrace(){
    return nbTrace;
}
void square::setnbTrace(int nb_Trace){
    nbTrace=nb_Trace;
}
int square::getMaxTrace(){
    return maxTrace;
}
int square::getNbAnts(){
    return nbAnts;
}
vector<square*> square::getneighbour(){
    return neighbour;
}
int square::getMaxAnts(){
    return maxAnts;
}
void square::addAntsToList(ants* newAnts) {
    listOfAnts.push_back(newAnts);
}
vector<ants *> square::getListOfAnts() {
    return listOfAnts;
}
void square::delAntsToList(ants * delants){
    listOfAnts.erase(remove(listOfAnts.begin(),listOfAnts.end(),delants),listOfAnts.end());

}
void square::evaporation() {
    if(nbTrace>0){
        nbTrace=nbTrace*EVAPORATION_VALUE;
    }

}
void square::resetNeighbour(){
    this->neighbour.clear();
}
