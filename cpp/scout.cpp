//
// Created by FU529AFA on 23/11/2020.
//

#include "../header/scout.h"
#include <iostream>



using namespace std;
scout::scout(){
    ages=0;
    underage=true;
}

scout::scout(square * Case){
    ages=0;
    underage=true;
    currentCase=Case;
}

void scout::eat(square* colonie){
    if(colonie->getUnitFoodCount()>=0.001){
        colonie->setUnitFoodCount(colonie->getUnitFoodCount()-0.001);
    }else{
        death();
    }

}
void scout::setUnderage(){
    if(ages>=2*24)
        underage=false;
}
void scout::move() {
    int index=10;
    bool mooved=false;
    vector<int> listeIndex;
    int i=0;
    for(i=0; i<currentCase->getneighbour().size(); i++){
         if(currentCase->getneighbour()[i]->getType()!=2){
             if(currentCase->getneighbour()[i]->isVisited()==false && currentCase->getneighbour()[i]->getNbAnts()<currentCase->getneighbour()[i]->getMaxAnts()){
                 listeIndex.push_back(i);
                 index=0;


        }
         }

    }
    if(index==10){
        for( i=0; i<currentCase->getneighbour().size(); i++){
            if(currentCase->getneighbour()[i]->getType()!=2 && currentCase->getneighbour()[i]->getNbAnts()<currentCase->getneighbour()[i]->getMaxAnts()){
                listeIndex.push_back(i);


            }
        }
    }
    if(listeIndex.size()==0){
        currentCase->delAntsToList(this);
        currentCase->addAntsToList(this);
        mooved=true;
    }
    if(mooved==false){
        index=rand() % listeIndex.size();
        currentCase->delAntsToList(this);
        currentCase->setNbAnts(currentCase->getNbAnts()-1);
        currentCase=currentCase->getneighbour()[listeIndex[index]];
        currentCase->addAntsToList(this);
        currentCase->setVisited(true);
        currentCase->setNbAnts(currentCase->getNbAnts()+1);

    }



}
void scout::death(){
    currentCase->delAntsToList(this);
    currentCase->setNbAnts(currentCase->getNbAnts()-1);
    delete this;
}
void scout::action() {

    if(ages==(24*365)){
        death();
    }else{
        setUnderage();

        if(!underage){
            move();
        }
    }
    ages++;

}

int scout::getAges(){
    return ages;
}
int scout::getType(){
    return 1;
}


