//
// Created by Admin on 25/11/2020.
//

#include "../header/spawn_square.h"

spawn_square::spawn_square(): square(){
    maxAnts = 100;
    maxTrace=0;
    unitFood_count = 2;
    visited = true;
    nbTrace = 0;
    nbAnts = 0;
}
spawn_square::spawn_square(std::vector<ants*> listOfAnts,std::vector<square*> neighbour,int nbAnts) : square(){
    this->listOfAnts = listOfAnts;
    this->neighbour = neighbour;
    maxAnts = 100;
    maxTrace=0;
    unitFood_count = 0;
    visited = true;
    nbTrace = 0;
    this->nbAnts = nbAnts;
}

void spawn_square::setUnitFoodCount(float unitFoodCount) {
    unitFood_count = unitFoodCount;
}

float spawn_square::getUnitFoodCount() {
    return unitFood_count;
}
int spawn_square::getType(){
    return 3;
}
