//
// Created by FU529AFA on 23/11/2020.
//

#include "../header/queen.h"
#include "../header/spawn_square.h"
#include "../header/worker.h"
#include "../header/soldier.h"
#include "../header/scout.h"
#include <iostream>



using namespace std;

queen::queen(){

    ages=0;
    mugged=false;

}
queen::queen(square * Case){
    ages=0;
    mugged=false;
    currentCase=Case;
}


void queen::action() {
    if(ages==24*365*10){
        death();
    }else{
        ages=ages+1;
        alive=true;
        if(mugged==false && ages%24==0){
            creatNewAnt();
            creatNewAnt();
        }
    }


}
void queen::creatNewAnt(){


    if(currentCase->getListOfAnts().size()==1){
        scout * newAnt=new scout(currentCase);
        currentCase->addAntsToList(newAnt);
        currentCase->setNbAnts(currentCase->getNbAnts()+1);
    }else{
        int proba=rand()%100;

        if(proba<=5){
            scout * newAnt=new scout(currentCase);
            currentCase->addAntsToList(newAnt);
            currentCase->setNbAnts(currentCase->getNbAnts()+1);
        }else if(proba>5 && proba<=20){
            soldier * newAnt=new soldier(currentCase);
            currentCase->addAntsToList(newAnt);
            currentCase->setNbAnts(currentCase->getNbAnts()+1);
        }else{
            worker* newAnt=new worker(currentCase);
            currentCase->addAntsToList(newAnt);
            currentCase->setNbAnts(currentCase->getNbAnts()+1);
        }
    }


}

void queen::eat(square* colonie){

    if(colonie->getUnitFoodCount()>=0.01){
        colonie->setUnitFoodCount(colonie->getUnitFoodCount()-0.01);
    }else{
        this->death();
    }

}

void queen::death(){
    alive=false;
}
int queen::isAlive() {
    return alive;
}
int queen::getAges(){
    return ages;
}
int queen::getType(){
    return 0;
}

