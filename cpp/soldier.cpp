//
// Created by FU529AFA on 23/11/2020.
//


#include "../header/spawn_square.h"
#include "../header/soldier.h"
#include <iostream>
#include <vector>

using namespace std;
 soldier::soldier(){

     ages=0;
     nbHourBeforeGoBack=goBackLimit*24;
     goback=false;

 }

soldier::soldier(square * Case){

    ages=0;
    goback=false;
    nbHourBeforeGoBack=goBackLimit*24;
    currentCase=Case;

}
void soldier::eat(square* colonie){
    if(colonie->getUnitFoodCount()>=0.001){
        colonie->setUnitFoodCount(colonie->getUnitFoodCount()-0.001);
    }else{
        death();
    }

}
void soldier::move() {
    int index=10;
    bool mooved=false;
    vector<int> listeIndex;
    int i=0;

    if(!goback){
        for(i=0; i<currentCase->getneighbour().size(); i++){
            if(currentCase->getneighbour()[i]->getType() != 2){
                if(currentCase->getneighbour()[i]->isVisited() && currentCase->getneighbour()[i]->getNbAnts()<currentCase->getneighbour()[i]->getMaxAnts()){
                    listeIndex.push_back(i);
                }
            }
        }
        if(listeIndex.size()==0){
            currentCase->delAntsToList(this);
            currentCase->addAntsToList(this);
        }else{
            index= rand() % listeIndex.size();
            mooved=true;
            memory.push(currentCase);
            currentCase->setNbAnts(currentCase->getNbAnts()-1);
            currentCase->delAntsToList(this);
            currentCase=currentCase->getneighbour()[listeIndex[index]];
            currentCase->addAntsToList(this);
            currentCase->setNbAnts(currentCase->getNbAnts()+1);
        }
    }else{
        if(!memory.empty()){
            currentCase->setNbAnts(currentCase->getNbAnts()-1);
            currentCase->delAntsToList(this);
            currentCase=memory.top();
            memory.pop();
            currentCase->setNbAnts(currentCase->getNbAnts()+1);
            currentCase->addAntsToList(this);


        }else{
            goback=false;
            nbHourBeforeGoBack=goBackLimit*24;
        }
    }





 }
void soldier::death(){
    currentCase->delAntsToList(this);
    currentCase->setNbAnts(currentCase->getNbAnts()-1);
    delete this;
 }
void soldier::action() {
   if(nbHourBeforeGoBack==0){
       goback=true;
   }
    if(ages==(24*365)){
        death();
    }else{
        move();
    }
    nbHourBeforeGoBack--;
    ages++;


}
int soldier::getAges(){
    return ages;
}
int soldier::getType(){
     return 3;
 }
