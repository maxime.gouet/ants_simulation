| Concept        | Mise en oeuvre           |
| ------------- |:-------------:|
| Surcharge des fonctions |X|
| Entrées / Sorties |X|
| Références |X|
| Constance |X|
| Opérateur de résolution de portée |X|
| Espaces de noms |X|
| Fichiers d’en-têtes et espaces de noms |X|
| Portée |X|
| Gestion mémoire : Allocation |X|
| Gestion mémoire : Libération |X|
| Classes et objets |X|
| Membres & partage |X|
| Accès aux Membres & Portée |X|
| Membres & Protection |X|
| Fonction membre : accès & pointeur this |X|
| Membres amis : friend ||
| Constructeur | X |
| Destructeur |X|
| Surcharge d’opérateurs |X|
| Conversion utilisateur ||
| Fonctions amies versus fonctions membres ||
| Fonctions inline ||
| Constructeur, initialisation, affectation |X|
| Constructeur, initialisation, affectation et allocation dynamique |X|
| Classes dérivées – Héritage simple |X|
| Héritage et constructeur |X|
| Conversion classes de base <-> classes dérivées |X|
| Dérivation et protection |X|
| Vers le polymorphisme |X|
| Liaison retardée, fonctions virtuelles |X|
| Classes abstraites et fonctions virtuelles pures |X|
| Type des objets et conversion explicite |X|
| Héritage multiple ||
| Généricité : Patrons de fonction ||
| Généricité : Patrons de classe ||
| Flots, Entrées /Sorties, Fichiers ||
| La Standard Template Library (STL) ||
| Programmation défensive – Assertions ||
| Programmation défensive – Exceptions ||